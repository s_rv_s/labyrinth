package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class Labyrinth {

    private final int wayValue = 0;
    private int labyrinth [][]= {{1, 1, 1, 1, 1, 1, 1},
                                 {1, 0, 0, 0, 1, 0, 1},
                                 {1, 0, 1, 0, 1, 0, 1},
                                 {1, 0, 1, 0, 0, 0, 1},
                                 {1, 0, 1, 1, 1, 0, 0},
                                 {0, 0, 1, 0, 0, 0, 1},
                                 {1, 1, 1, 1, 1, 1, 1}};
    private Point labyrinthSize = new Point(labyrinth[0].length, labyrinth.length);
    private Stack<CharacterStep> way = new Stack<>();

    private Point getNextPoint(Point point, Direction direction) {
        Point nextPoint = null;
        switch (direction) {
            case LEFT: {
                nextPoint = new Point(point.getX()-1, point.getY());
                break;
            }
            case RIGTH: {
                nextPoint = new Point(point.getX()+1, point.getY());
                break;
            }
            case UP: {
                nextPoint = new Point(point.getX(), point.getY()-1);
                break;
            }
            case DOWN: {
                nextPoint = new Point(point.getX(), point.getY()+1);
                break;
            }
        }
        if(nextPoint.getX() < 0 || nextPoint.getY() < 0
                || nextPoint.getX() >= labyrinthSize.x || nextPoint.getY() >= labyrinthSize.getY()
                || labyrinth[nextPoint.getX()][nextPoint.getY()] != wayValue) {
            nextPoint = null;
        }
        return nextPoint;
    }

    private CharacterStep findStartStep() {
        Point point = null;
        int idx;
        for (idx = 0; idx < labyrinthSize.getY(); idx++){
            if(labyrinth[idx][0] == wayValue) {
                point = new Point(idx,0);
            }
        }
        if(null != point)
            for (idx = 0; idx < labyrinthSize.getY(); idx++){
                if(labyrinth[0][idx] == wayValue) {
                    point = new Point(0, idx);
                }
            }
        return new CharacterStep(point);
    }

    private boolean isExitPoint(Point point) {
        return (point.getX() == 0 || point.getY() == 0
                || point.getX() == labyrinthSize.getX() -1 || point.getY() == labyrinthSize.getY()-1);
    }

    private boolean isExistStepOnwWay(Point point) {
        return way.stream().anyMatch( step -> step.point.equals(point));
    }

    private List<Point> findAllowedSteps(CharacterStep step) {
        List<Point> allowedSteps = new ArrayList<>();

        for(Direction nextDirection: Direction.values()){
            Point nextPoint = getNextPoint(step.getPoint(), nextDirection);
            if(null != nextPoint && !isExistStepOnwWay(nextPoint)){
                allowedSteps.add(nextPoint);
            }
        }
        return allowedSteps;
    }

    private CharacterStep popLastStepWithAllowedPoint(){
        if(way.isEmpty())
            return null;
        else {
            CharacterStep step = way.peek();
            while (!step.hasAllowedPoint()) {
                way.pop();
                if(!way.isEmpty()){
                    step = way.peek();
                } else {
                    step = null;
                    break;
                }
            }
            return step;
        }
    }

    void run() {
        CharacterStep startStep = findStartStep();
        startStep.addAllowedPoints(findAllowedSteps(startStep));
        way.add(startStep);

        CharacterStep step = startStep;
        while (null != step && step.hasAllowedPoint()) {
            CharacterStep nextStep = new CharacterStep(step.getAllowedPoint());
            nextStep.addAllowedPoints(findAllowedSteps(nextStep));

            if(isExitPoint(nextStep.getPoint())) {
                way.add(nextStep);
                break;
            } else {
                if (!nextStep.hasAllowedPoint()) {
                    step = popLastStepWithAllowedPoint();
                    continue;
                } else {
                    way.add(nextStep);
                    step = nextStep;
                }
            }
        }
        System.out.println(way.stream().map(CharacterStep::toString).collect(Collectors.toList()));
    }
}
