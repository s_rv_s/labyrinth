package com.company;

public class Point {
    int x;
    int y;

    Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        } else if (!(obj instanceof Labyrinth.Point)) {
            return false;
        } else {
            Labyrinth.Point o = (Labyrinth.Point) obj;
            return this.x == o.x
                    && this.y == o.y;
        }
    }

    @Override
    public int hashCode() {
        return (x +1) * 1000  + y+1;
    }

    @Override
    public String toString() {
        return String.format("-> %d:%d ", y+1,x+1);
    }
}
