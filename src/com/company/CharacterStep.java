package com.company;

import java.util.List;
import java.util.Stack;

public class CharacterStep {
    Point point;
    Stack<Point> allowedPoints = new Stack<>();

    CharacterStep(Point point){
        this.point = point;
    }

    public Point getPoint() {
        return point;
    }

    public void addAllowedPoints(List<Point> allowedPoints) {
        this.allowedPoints.addAll(allowedPoints);
    }
    public void removeAllowedPoint(Point allowedPoint) {
        this.allowedPoints.remove(allowedPoints);
    }

    public Point getAllowedPoint() {
        return allowedPoints.pop();
    }

    public boolean hasAllowedPoint() {
        return !allowedPoints.empty();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        } else if (obj == null || !(obj instanceof Labyrinth.CharacterStep)) {
            return false;
        } else {
            Labyrinth.CharacterStep o = (Labyrinth.CharacterStep) obj;
            return this.getPoint().equals(o.getPoint());
        }
    }

    @Override
    public int hashCode() {
        return getPoint().hashCode();
    }

    @Override
    public String toString() {
        return point.toString();
    }
}
